/// <reference types="cypress" />

describe('Automation practice', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.get('.logo').should('be.exist').and('be.visible');
  });
  
  it('Busca inválida', () => {
    cy.get('#search_query_top').type('Teste');
    cy.get('.button-search').click();
    cy.get('.alert').should('be.exist').and('be.visible');
  });
  
  it('Adição de produto ao carrinho com sucesso', () => {
    cy.get('.ajax_cart_no_product').should('have.text', '(empty)').and('be.visible');
    cy.get('#search_query_top').type('Shirt');
    cy.get('.button-search').click();
    cy.get('.ajax_block_product').trigger('mouseover');
    cy.get('.ajax_add_to_cart_button').first().click();
    cy.get('a.btn.button[title="Proceed to checkout"]').click();
    cy.get('#summary_products_quantity').should('include.text', '1').and('be.visible');
  });
});
